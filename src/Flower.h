#pragma once
#ifndef _FLOWER // if this class hasn't been defined, the program can define it
#define _FLOWER // by using this if statement you prevent the class to be called more than once which would confuse the compiler
#include "ofMain.h" // we need to include this to have a reference to the openFrameworks framework
#include "ofxDSHapVideoPlayer.h"

class Flower {

public: // place public functions or variables declarations here

		// methods, equivalent to specific functions of your class objects
	void setup(float _x, float _y, float _rotSpeedMin, float _rotSpeedMax, float _scaleMin, float _scaleMax);	// setup method, use this to setup your object's initial state
	void update();  // update method, used to refresh your objects properties
	void draw();    // draw method, this where you'll do the object's drawing

	// variables
	float x;        // position
	float y;
	float rotationOffset;   // rotation
	float rotationMult; //rotation multiplier in -1/+ range
	float rotMin, rotMax;
	float scaleMin, scaleMax;
	int dim;        // size
	//ofVideoPlayer flowerMovie;  // flower
	//ofImage test;
	float videoPosition; //change the rate of video for each flower
	ofxDSHapVideoPlayer hapFlower;

	Flower();  // constructor - used to initialize an object, if no properties are passed the program sets them to the default value
private: // place private functions or variables declarations here
}; // don't forget the semicolon!
#endif