#pragma once

#include "ofMain.h"
#include "ofxPoissonDiskSampling.h"
#include "ofxOpenCv.h"
#include "ofxGui.h"
#include "ofxDSHapVideoPlayer.h"
#include "Flower.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		float tondoRadius, tondoCenterX, tondoCenterY;
		int numOfFlowers; //how many flowers
		
		vector <ofVec2f> tempPoints; //store the points that are inside the circle
		vector <Flower> instanceFlowers; //change this into a variable? can't do this in h

		//camera motion detection
		ofVideoGrabber			vidGrabber;

		ofxCvColorImage			colorImg;
		ofxCvGrayscaleImage 	grayImage;
		ofxCvGrayscaleImage 	grayBg; //background
		ofxCvGrayscaleImage 	grayDiff; //difference
		ofxCvFloatImage			grayImageFloat;
		ofxCvFloatImage			grayBgFloat; //Background
		//ofPixels				grayBigPixels;
		ofPixels				pixels;

		ofxCvGrayscaleImage     grayBig; //big full screen image

		//trying it just as ofImage instead of ofxOpenCv
		//ofImage colorImg;
		//ofImage grayImage;
		//ofImage grayBg; //background
		//ofImage grayDiff; //difference
		//ofImage grayBig; //big full screen image

		int						thresholdValue;
		bool					bLearnBackground;

		//image placement test variables
		//ofImage test;
		//ofVideoPlayer flowerMovie;  // flower
		//ofxDSHapVideoPlayer flowerMovie;

		// a vector with a dynamic number of videos, based on the content of a directory:
		ofDirectory dir;
		vector<ofxDSHapVideoPlayer> videos;
		int currentVideo;

		////////////UI//////////////////////

		bool bHide;

		ofxFloatSlider blurAmount;
		ofxFloatSlider brightness;
		ofxFloatSlider contrast;
		ofxVec2Slider position;
		ofxIntSlider iResolution;
		ofxToggle cameraMirror;
		ofxToggle cameraDisplay;
		ofxToggle thresholdToggle;
		ofxIntSlider threshold;
		ofxFloatSlider scaleMin;
		ofxFloatSlider scaleMax;
		ofxFloatSlider rotSpeedMin;
		ofxFloatSlider rotSpeedMax;

		ofxLabel flowerCtrl, camCtrl;

		ofxPanel gui;

		//poisson
	private:
		vector<ofVec2f> m_samples;
		float m_density;
		
		
};
