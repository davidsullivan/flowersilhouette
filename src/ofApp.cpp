#include "ofApp.h"

/*--------------------------------------------------------------
//layout points in a circle (using poisson distribution), points would have a radius of non-overlapping, like a force field 
//create vector of videos
//create vector of flower instances from points (loc) and videos from flower class (pos from points, video, frame, scale, rotation, video speed)
//movement detection with background subtraction

//put the whole creation of the two vectors into a function, so it can be called from a key command after changing the variables

scale up the camera detection image to the size of the screen
find the pixel value that correspondes to a flower instance point
use the value to drive the speed of that movie

hap or hpv?
hap doesn't work inside the flower class
so need a vector of flower movie players, each with a different video file from the directory
alpha or blend mode?

///variables////
scale min max
rotation min max

//hm, you can also use a vector with pointers to images
vector <ofImage*> Pics;

//create & load with

Pics.push_back(new ofImage());
Pics.back()->loadImage(path1);
Pics.push_back(new ofImage());
Pics.back()->loadImage(path2);
Pics.push_back(new ofImage());
Pics.back()->loadImage(path3);

//draw them

for(int i=0;i<Pics.size();i++) {
Pics[i]->draw(0, 0);
}

//this has several advantages & disadvantages�(pointers, passing vale to other classes, boa�)
*/

//--------------------------------------------------------------
void ofApp::setup(){
	ofEnableAntiAliasing();
	ofEnableSmoothing();
	float w = ofGetWidth();
	float h = ofGetHeight();

	tondoRadius = (h * 0.5) - 10;
	tondoCenterX = w / 2;
	tondoCenterY = h / 2;

	//put all of this point in a circle code into its own function, so it can be called at will
	m_density = min<float>(w, h) * 0.08; //turn this number (0.09) into a variable (between 0.2 and 0.08), lower number means more instances
	m_samples = ofxPoissonDiskSampling::sample2D(w, h, m_density, false);

	for (int i = 0; i < m_samples.size(); i++) {
		float x = m_samples[i].x;
		float y = m_samples[i].y;
		if (ofDist(x, y, tondoCenterX, tondoCenterY) <= tondoRadius) {
			// point is outside of circle
			ofVec2f p;
			p.set(x, y);
			tempPoints.push_back(p);
		}
	}

	/*
	*/

	////opencv/////////////////////////////////////////////
	vidGrabber.setVerbose(true);
	vidGrabber.setDeviceID(0); //external camera = 1, internal = 0
	vidGrabber.setup(320, 240);

	colorImg.allocate(320, 240);
	grayImage.allocate(320, 240);
	grayBg.allocate(320, 240);
	grayDiff.allocate(320, 240);

	grayBig.allocate(w, h);//ofxCVGrayScaleImage
	pixels.allocate(w, h, OF_PIXELS_MONO);
	//image.allocate(videoWidth, videoHeight, OF_IMAGE_COLOR);//ofImage

	bLearnBackground = true;
	thresholdValue = 80;

	//////UI///////////////////////////////////////////////////
	//add gui -- sliders - threshold        booleons - mirror, camera image
	//////ofxgui
	gui.setup("FLORAL SILHOUETTE", "settings.xml"); //
	gui.setPosition(10, 200);
	gui.add(camCtrl.setup(ofToString("CAMERA CONTROL")));
	gui.add(cameraDisplay.setup("Camera Display", true));
	gui.add(cameraMirror.setup("Camera Mirror", true));
	gui.add(thresholdToggle.setup("Threshold Toggle", false));
	gui.add(threshold.setup("Threshold Amount", 100, 0, 255));
	gui.add(brightness.setup("Brightness Amount", 0, 0, 1)); //what is the range for brightness and contrast????
	gui.add(contrast.setup("Contrast Amount", 0, 0, 1));
	//gui.add(blurAmount.setup("Blur Amount", 11, 0, 40));
	//position video overlay
	gui.add(position.setup("Camera Position", ofVec2f(0, ofGetHeight()*.65), ofVec2f(0, 0), ofVec2f(ofGetWidth(), ofGetHeight())));
	gui.add(flowerCtrl.setup(ofToString("FLOWER CONTROL")));
	gui.add(scaleMin.setup("Scale Min", 0.5, 0.0, 2.0));
	gui.add(scaleMax.setup("Scale Max", 1.5, 0.1, 2.0));
	gui.add(rotSpeedMin.setup("Rotation Speed Min", -0.29, -0.1, 0.0));
	gui.add(rotSpeedMax.setup("Rotation Speed Max", 0.29, 0, 0.1));

	bHide = false;

	///CREATE THE VECTOR OF FLOWER INSTANCES AFTER THE GUI SINCE THE GUI VARIABLES ARE PASSED IN TO THE INSTANCES
	for (int i = 0; i < tempPoints.size(); i++) {
		Flower tempFlower;							// create the flower object
		tempFlower.setup(tempPoints[i].x, tempPoints[i].y, rotSpeedMin, rotSpeedMax, scaleMin, scaleMax);	// setup its initial state
		instanceFlowers.push_back(tempFlower);
	}

	//AFTER THE FLOWER INSTANCES ARE MADE THAN THE CORRESPONDING VIDEO VECTOR IS MADE
	///////directory to video vector/////////////////////////
	dir.listDir("videos/");
	dir.allowExt("avi");
	dir.sort(); // in linux the file system doesn't return file lists ordered in alphabetical order

	//allocate the vector to have as many videos as the flower vector
	if (dir.size()) {
		videos.assign(instanceFlowers.size(), ofxDSHapVideoPlayer());
	}
	currentVideo = 0;
	// you can now iterate through the files and load them into the videos vector
	for (int i = 0; i < instanceFlowers.size(); i++) {
		currentVideo = ofRandom(0, (int)dir.size());
		videos[i].load(dir.getPath(currentVideo));
		videos[i].setLoopState(OF_LOOP_NONE);
		videos[i].play();
	}
	//flowerMovie.load("videos/mag12_smallHapq.mov");
	//flowerMovie.setLoopState(OF_LOOP_NONE);
	//flowerMovie.play();
	//test.load("directions_128.png");


}

//--------------------------------------------------------------
void ofApp::update() {

	//camera
	// Ask the camera to update itself.
	vidGrabber.update();

	if (vidGrabber.isFrameNew()) { // If there is fresh data...

								  // Copy the data from the video into an ofxCvColorImage
		colorImg.setFromPixels(vidGrabber.getPixels());

		// Make a grayscale version of colorImg in grayImage
		grayImage = colorImg;
		//grayImage.setImageType(OF_IMAGE_GRAYSCALE);   // now I am grayscale;

		//adaptive background subtraction so the background changes over time
		//grayBg = (0.99 *= grayBg) + (0.01 *= grayImage);
		//or
		grayImageFloat = grayImage;
		grayImageFloat *= 0.01;
		grayBgFloat = grayBg;
		grayBgFloat *= 0.99;
		grayBgFloat += grayImageFloat;
		grayBg = grayBgFloat;

		// If it's time to learn the background;
		// copy the data from grayImage into grayBg
		if (bLearnBackground == true) {
			grayBg = grayImage; // Note: this is 'operator overloading'
			bLearnBackground = false; // Latch: only learn it once.
		}

		// Take the absolute value of the difference 
		// between the background and incoming images.
		grayDiff.absDiff(grayBg, grayImage);

		// Perform an in-place thresholding of the difference image.
		if (thresholdToggle) {
			grayDiff.threshold(threshold);
		}

		//grayDiff.blurGaussian(blurAmount);
		grayDiff.brightnessContrast(brightness, contrast);
		//grayDiff.adaptiveThreshold();
		//grayDiff.contrastStretch();
		grayDiff.mirror(0, cameraMirror); //boolean vertical, horizontal
					
		//	scale the camera image to the size of the screen
		grayBig.scaleIntoMe(grayDiff); // from 320,240 to screen size
		
		pixels = grayBig.getPixels();
		
		//	use a for loop to find the pixel value at the position of each flowerInstance
		//	use the pixelvalue to set the speed/position of the flowerInstance movie
		//Getting video frame size for formulas simplification
		int w = ofGetWidth();
		int h = ofGetHeight();
		//Scan all the pixels
		for (int i = 0; i < instanceFlowers.size(); i++) {
			float x = instanceFlowers[i].x;
			float y = instanceFlowers[i].y;

			//get the gray value from the difference image for the current pixel
			//ofColor GrayTemp = grayBigPixels.getPixelIndex(x, y); //is there something like getPixelIndex in ofxCv, or convert grayBig to ofPixels object
			//ofColor GrayTemp = grayBig.getPixels()[x + y * grayBig.width];
			ofColor GrayTemp = pixels.getColor(x, y);
			//get gray pixel brightness
			float luma;
			luma = GrayTemp.getBrightness();
			//map gray to speed of video
			luma = ofMap(luma, 10, 245, 1, 0, true);

////////////it looks like the luma values are stil right, but no longer drive the setposition after a change is made//////////////////////////
//////////// problem with setPosition updating, setSpeed does not work, so i rolled my own setPosition

			//set the movei speed of this flower instance
			//videos[i].setSpeed(luma);
			//instanceFlowers[i].dim = luma * 80;
			//videos[i].setPosition(luma);
			videos[i].setFrame(int(videos[i].getTotalFrames()*luma));
			//instanceFlowers[i].videoPosition = luma; this didn't change anything
			videos[i].update();
		}
		
	}

	//flowerMovie.update();
	//std::cout << "video vector size: " << videos.size() << endl;
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(0);
	//ofSetColor(220, 0, 50);
	//ofFill();
	//draw circles where the points are positioned
	//for (ofVec2f v : tempPoints) {
	//	ofDrawCircle(v.x, v.y, 5);
	//}

	//ofSetColor(0, 90, 180);
	//ofNoFill();
	//for (ofVec2f v : tempPoints) {
	//	ofDrawCircle(v.x, v.y, m_density * 0.5);
	//}
	//ofDrawCircle(tondoCenterX, tondoCenterY, tondoRadius);
	//ofEnableBlendMode(OF_BLENDMODE_ADD);
	for (int i = 0; i < instanceFlowers.size(); i++) {
		ofPushMatrix();
		ofTranslate(instanceFlowers[i].x, instanceFlowers[i].y); //translate 0, 0 to the instance point
		ofRotateDeg((ofGetFrameNum() + instanceFlowers[i].rotationOffset) * instanceFlowers[i].rotationMult);//rotate from centre
		//test.draw(-instanceFlowers[i].dim/2, -instanceFlowers[i].dim/2, instanceFlowers[i].dim, instanceFlowers[i].dim); //minus half the dimensions to center it
		videos[i].draw(-instanceFlowers[i].dim / 2, -instanceFlowers[i].dim / 2, instanceFlowers[i].dim, instanceFlowers[i].dim);
		ofPopMatrix();
	}
	//ofDisableBlendMode();
	//flowerMovie.draw(0, 0, 136, 100);
	//draw camera
	if (cameraDisplay) {
		grayDiff.draw(position->x, position->y);

		// finally, a report:
		ofSetHexColor(0xffffff);
		stringstream reportStr;
		reportStr << "bg subtraction" << endl
			<< "press ' ' to capture bg" << endl
			<< "press 'g' to toggle gui panel" << endl
			<< "threshold " << threshold << " (press: +/-)" << endl
			<< "fps: " << ofGetFrameRate();
		ofDrawBitmapString(reportStr.str(), 20, 20);
	}
	//gui
	if (!bHide) {
		gui.draw();
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key) {
	case ' ':
		bLearnBackground = true;
		break;
	case '=':
		threshold = threshold + 1;
		if (threshold > 255) threshold = 255;
		break;
	case '-':
		threshold = threshold - 1;
		if (threshold < 0) threshold = 0;
		break;
	case 'g':
		bHide = !bHide;
		break;
	case 's':
		gui.saveToFile("settings.xml");
		break;
	case 'l':
		gui.loadFromFile("settings.xml");
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
