#include "Flower.h"
Flower::Flower() {
}

void Flower::setup(float _x, float _y, float _rotSpeedMin, float _rotSpeedMax, float _scaleMin, float _scaleMax) {
	x = _x;      // positioning
	y = _y;
	rotMin = _rotSpeedMin;
	rotMax = _rotSpeedMax;
	scaleMin = _scaleMin;
	scaleMax = _scaleMax;
	rotationOffset = ofRandom(0, 360);           // and random rotation
	rotationMult = ofRandom(rotMin, rotMax);
	videoPosition = 1;
	//test.load("directions_128.png");

	dim = ofRandom(scaleMin, scaleMax) * 100; // flowerMovie.getHeight(); //scale

	// set flower movie to one of the movies from the vector
	//hapFlower.load("sample-1080p30-HapA.avi"); //use vector of filenames to build path
	//hapFlower.setLoopState(OF_LOOP_NORMAL);
	//hapFlower.play();
}

void Flower::update() {
	//hapFlower.update();
}

void Flower::draw() {
	//need to move the translation grid to the instance point , rotate, and than center the image on the point
	ofPushMatrix();
		ofTranslate(x, y); //translate 0, 0 to the instance point
		ofRotateDeg((ofGetFrameNum() + rotationOffset) * rotationMult);//rotate from centre
		///test.draw(-dim/2, -dim/2, dim, dim); //minus half the dimensions to center it
		//hapFlower.draw(-dim / 2, -dim / 2, dim, dim);
	ofPopMatrix();
}